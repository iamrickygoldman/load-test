using System;
using System.Net.Sockets;
using System.Text;

namespace LoadTest
{
	public class Simulator
	{
		const string END = "`";
		const int NULL_INT = Int32.MinValue;

		public User User {get; set;}

		public Socket Handler {get; set;}

		public int IdleCount {get; set;} = 0;

		public bool Active {get; set;} = true;

		public int MatchId {get; set;} = 0;

		public bool OpponentConnected {get; set;} = false;
		public bool SelfConnected {get; set;} = false;

		public Simulator(User user, Socket handler)
		{
			User = user;
			Handler = handler;
		}

		public void Send(string text)
        {
        	byte[] msg = Encoding.ASCII.GetBytes(text + END);

        	try {
        		int bytesSend = Handler.Send(msg);
        		Console.WriteLine("User {1} Sent: {0}", Encoding.ASCII.GetString(msg,0,bytesSend), User.Id);
        	} catch (Exception e) {
        		Console.WriteLine(e.ToString());
        	}
        }

		public string ExtractString(string text, string marker)
		{
			int startIndex = text.IndexOf("{" + marker);
			if (startIndex < 0)
			{
				return "";
			}
			int stopIndex = text.IndexOf(marker + "}");
			if (stopIndex - startIndex - 2 < 1)
			{
				return "";
			}
			return text.Substring(startIndex + 2, stopIndex - startIndex - 2);
		}

		public int ExtractInt(string text, string marker)
		{
			int startIndex = text.IndexOf("{" + marker);
			if (startIndex < 0)
			{
				return NULL_INT;
			}
			int stopIndex = text.IndexOf(marker + "}");
			if (stopIndex - startIndex - 2 < 1)
			{
				return NULL_INT;
			}
			string extractedString = text.Substring(startIndex + 2, stopIndex - startIndex - 2);
			int extractedInt;
			if (Int32.TryParse(extractedString, out extractedInt))
			{
				return extractedInt;
			}
			return NULL_INT;
		}

		public string PackString(string data, string marker)
		{
			return "{" + marker + data + marker + "}";
		}

		public string PackString(int data, string marker)
		{
			return "{" + marker + data + marker + "}";
		}

		public void Process(string text)
		{
			if (text == "#" + END)
			{
				Act();
			}
			if (text.Length < 3)
			{
				return;
			}
			text = text.Substring(0, text.Length - 1);
			char type = text[0];
			char action = text[1];

			switch (type)
			{
				case '#':
					switch (action)
					{
						case 'm':
							MatchId = ExtractInt(text, "M");
                        	if (MatchId == NULL_INT) { return; }
                        	User.Status = UserStatus.Loading;
							break;
						case 'l':
							OpponentConnected = true;
							if (SelfConnected)
							{
								User.Status = UserStatus.Playing;
							}
							break;
					}
					break;
			}
		}

		public void Act()
		{
			if (IdleCount++ < 3)
			{
				return;
			}
			if (User.Status == UserStatus.LoggedOut)
			{
				Send("#I");
				User.Status = UserStatus.Waiting;
			}
			else if (User.Status == UserStatus.Waiting)
			{
				Send("#m");
				User.Status = UserStatus.Matchmaking;
			}
			else if (User.Status == UserStatus.Loading)
			{
				Send("#l" + PackString(MatchId, "M"));
				SelfConnected = true;
				if (OpponentConnected)
				{
					User.Status = UserStatus.Playing;
				}
			}
			else if (User.Status == UserStatus.Playing)
			{
				Random random = new Random();
				int red = random.Next(0,256);
				int green = random.Next(0,256);
				int blue = random.Next(0,256);
				Send("!t" + PackString(MatchId, "M") + PackString(red, "R") + PackString(green, "G") + PackString(blue, "B"));
			}
			IdleCount = 0;
		}
	}
}