namespace LoadTest
{
	public class User
	{
		public int Id {get; set;}

		public UserStatus Status {get; set;} = UserStatus.LoggedOut;

		public User(int id)
		{
			Id = id;
		}
	}
}

public enum UserStatus
{
	LoggedOut,
	Waiting,
	Matchmaking,
	Loading,
	Playing,
}