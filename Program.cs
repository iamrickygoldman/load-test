﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.FileExtensions;
using Microsoft.Extensions.Configuration.Json;

using LoadTest;

namespace tcp_user
{
    class Program
    {
    	const int LOAD = 1000;
        const string END = "`";

        static void Main(string[] args)
        {
            bool endProgram = false;
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("configuration.json");
            IConfigurationRoot configuration = builder.Build();
            IPAddress ipAddress = IPAddress.Parse(configuration.GetSection("Network").GetSection("Address").Value);
            int port = Int32.Parse(configuration.GetSection("Network").GetSection("Port").Value);

            try {
            	IPEndPoint remoteEP = new IPEndPoint(ipAddress,port);
            	for (int i = 0; i < LOAD; i++)
	            {
	            	User user = new User(i);

	            	try {
                		Socket socket = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                		Simulator simulator = new Simulator(user, socket);

                		try {
                			socket.Connect(remoteEP);
                			Console.WriteLine("User {1} connected to {0}", socket.RemoteEndPoint.ToString(), user.Id);

                			Thread listenThread = new Thread(() =>
	                		{
	                			Thread.CurrentThread.IsBackground = true;

	                			byte[] bytes = new Byte[1024];
		                        string received = null;

		                        Console.WriteLine("User {1} listening to {0}", socket.RemoteEndPoint.ToString(), user.Id);

		                        while (!endProgram && simulator.Active)
		                        {
		                            received = null;
		                            while (true)
		                            {
		                                int bytesRec = socket.Receive(bytes);
		                                received += Encoding.ASCII.GetString(bytes, 0, bytesRec);
		                                if (received.IndexOf(END) > -1)
		                                {
		                                    break;
		                                }
		                            }
		                            //Console.WriteLine("User {1} Received: {0}", received, user.Id);
		                            simulator.Process(received);
		                        }

		                        socket.Shutdown(SocketShutdown.Both);
		                        socket.Close();
	                		});

	                		listenThread.Start();

		                } catch (Exception e) {
		                    Console.WriteLine(e.ToString());
		                }

	            	} catch (Exception e) {
		                Console.WriteLine(e.ToString());
		            }
		            Thread.Sleep(25);
	            }
            } catch (Exception e) {
                Console.WriteLine(e.ToString());
            }

            while (!endProgram)
            {
            	Thread.Sleep(5000);
            }
        }
    }
}
