publish:
dotnet publish --self-contained --runtime linux-x54

run:
dotnet run

build:
dotnet build

add package:
dotnet add package <name>

close port:
fuser -k 8080/tcp